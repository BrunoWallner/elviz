mod lissajous;
mod mesh;
mod spectrum;

use mesh::Mesh;
use std::sync::mpsc;

use crate::app::Instruction;
use crate::config;
use std::thread;

use audioviz::io::{Device, Input};
use audioviz::spectrum::config::ProcessorConfig;
use audioviz::spectrum::{config::StreamConfig, stream::Stream};

use std::time::Duration;
use std::time::Instant;

pub fn init(tx: mpsc::Sender<Instruction>, tick_rate: Duration) {
    thread::spawn(move || {
        let mut audio_input = Input::new();

        let (channel_count, sampling_rate, audio_receiver) =
            match audio_input.init(&Device::DefaultInput, Some(512)) {
                Ok(o) => o,
                Err(_) => {
                    log::warn!("default input not found, manual selection...");
                    // device selection
                    let devices = audio_input.fetch_devices().unwrap();
                    for (id, device) in devices.iter().enumerate() {
                        println!("{id}\t{device}");
                    }
                    let id: usize = input("id: ").parse().unwrap_or(0);
                    audio_input
                        .init(&Device::Id(id), Some(512))
                        .expect("init audio backend")
                }
            };

        let config = config::get();

        let mut stream_config = StreamConfig {
            channel_count,
            fft_resolution: config.spectrum.fft_resolution,
            gravity: config.spectrum.gravity,
            processor: ProcessorConfig {
                resolution: config.spectrum.amount,
                volume: config.volume,
                frequency_bounds: config.spectrum.frequency_bounds,
                interpolation: config.spectrum.interpolation,
                ..ProcessorConfig::default()
            },
            ..StreamConfig::default()
        };

        stream_config.channel_count = channel_count;
        let stream: Stream = Stream::new(stream_config);
        let mut spectrum = spectrum::Spectrum { stream };

        let mut lissajous = lissajous::Lissajous::new(channel_count as usize, sampling_rate as f32);

        let spectrum_enabled = config.spectrum.enabled;
        let lissajous_enabled = config.lissajous.enabled;

        loop {
            let start = Instant::now();

            let new_data = match audio_receiver.try_pull_data() {
                Some(d) => d,
                None => Vec::new(),
            };
            if spectrum_enabled {
                spectrum.push_data(new_data.clone());
            }
            if lissajous_enabled {
                lissajous.push_data(&new_data);
            }

            let mut mesh = Mesh::new(Vec::new(), Vec::new());
            if spectrum_enabled {
                mesh.merge(spectrum.update());
            }
            if lissajous_enabled {
                mesh.merge(lissajous.update());
            }

            match tx.send(Instruction::SetMesh(mesh)) {
                Ok(_) => {}
                Err(_) => break,
            };

            let elapsed = start.elapsed();
            if tick_rate > elapsed {
                let to_sleep = tick_rate - elapsed;
                thread::sleep(to_sleep);
            }
        }
    });
}

use std::io::Write;

fn input(print: &str) -> String {
    print!("{}", print);
    std::io::stdout().flush().unwrap();
    let mut input = String::new();

    std::io::stdin()
        .read_line(&mut input)
        .ok()
        .expect("Couldn't read line");

    input.trim().to_string()
}
