use audioviz::lissajous::StereoDelayer;
use crate::graphics::Mesh;
use super::mesh;
use crate::config;

use audioviz::processor::{Plugin, MultiChannelProcessor, Lowpass};
use audioviz::utils::seperate_channels;

const PLUGINS: &[Option<Plugin>] = &[
    None,
    Some(Plugin::Lowpass(Lowpass::new(400.0, 500.0))),
];

pub struct Lissajous {
    stereo_delayer: StereoDelayer,
    sampling_rate: f32,
    channel_count: usize,
}
impl Lissajous {
    pub fn new(channel_count: usize, sampling_rate: f32) -> Self {
        let config = config::get().lissajous;
        Self {
            stereo_delayer: StereoDelayer::new(channel_count, 0.9, 0.1, config.buffer_length),
            sampling_rate,
            channel_count,
        }
    }

    pub fn update(&mut self) -> Mesh {
        let config = config::get().lissajous;

        let mut data = self.stereo_delayer.update();
        // reduce resolution
        for _ in 0..config.resolution_reduction {
            for i in 0..data.len() / 4 {
                data.remove(i * 2);
            }
        }

        let mut points: Vec<Vec<[f32; 2]>> = Vec::new();
        for plugin in PLUGINS {
            let mut temp_data: Vec<f32> = Vec::new();
            for d in data.iter() {
                temp_data.push(d[0]);
                temp_data.push(d[1]);
            }
            let data = if let Some(plugin) = plugin {
                let mut processor = MultiChannelProcessor {
                    data: temp_data,
                    sampling_rate: self.sampling_rate as f32,
                    plugins: vec![
                        *plugin
                    ],
                    channel_count: self.channel_count as usize,
                };
                processor.process();
                processor.data
            } else {
                temp_data
            };

            let out = seperate_channels(&data, self.channel_count);
            let mut temp_points: Vec<[f32; 2]> = Vec::new();
            for i in 0..out[0].len() {
                temp_points.push([
                    out[0][i],
                    out[1][i],
                ]);
            }
            points.push(temp_points);
        }


        mesh::from_lissajous(points)


    }

    pub fn push_data(&mut self, data: &[f32]) {
        let config = config::get();
        let data: Vec<f32> = data
            .iter()
            .map(|x| x * config.volume)
            .collect();
        self.stereo_delayer.push_data(&data);
    }
}