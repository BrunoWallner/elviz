pub use crate::graphics::{Mesh, Vertex};
use audioviz::spectrum::Frequency;
use crate::config;

pub fn from_frequencies(frequencies: Vec<Frequency>) -> Mesh {
    let config = config::get().spectrum;
    let a = config.alpha;
    let y_multiplier = config.y_multiplier;
    let y_offset = config.y_offset;
    let width_diff = 1.0 - config.bar_width;


    let color = [1.0, 1.0, 1.0, a];
    let freq_len = frequencies.len();
    let mut freqs = frequencies.iter().peekable();
    let mut vertices: Vec<Vertex> = Vec::new();
    let mut indices: Vec<u32> = Vec::new();

    // // bass modifier
    // if freq_len > 0 {
    //     let mut bass_vol = 0.0;
    //     let mut total = 0.0;
    //     for i in ((freq_len / 2) - (freq_len / 8))..((freq_len / 2) + freq_len / 8) {
    //         bass_vol += frequencies[i].volume;
    //         total += 1.0;
    //     }
    //     bass_vol /= total;
    //     let bass_vol = ((bass_vol - 0.2) * 2.0).clamp(0.0, 1.0);
    //     color[1] += bass_vol / 2.0;
    //     color[2] += bass_vol;
    //     color[0] -= bass_vol / 1.5;
            
    // }


    let mut x = 0.0;
    let mut color_offset = 0.0;
    loop {
        let f1 = match freqs.next() {
            Some(f) => f,
            None => break,
        };

        let x1 = ((x * 2.0 + width_diff) / freq_len as f32) - 1.0;
        let x2 = ((x * 2.0 + 2.0 - width_diff) / freq_len as f32) - 1.0;
        let y = f1.volume * y_multiplier - 1.0 + y_offset;

        let offset = vertices.len() as u32;
        let mut color = color;
        color[2] += color_offset / freq_len as f32;

        vertices.push(Vertex {position: [x1, y, 0.0], color});
        vertices.push(Vertex {position: [x2, y, 0.0], color});   
        vertices.push(Vertex {position: [x1, -1.0 + y_offset, 0.0], color});   
        vertices.push(Vertex {position: [x2, -1.0 + y_offset, 0.0], color});

        indices.push(offset + 1);
        indices.push(offset + 0);
        indices.push(offset + 2);

        indices.push(offset + 1);
        indices.push(offset + 2);
        indices.push(offset + 3);

        x += 1.0;
        if x <= (freq_len as f32 / 2.0) {
            color_offset += 0.66;
        } else {
            color_offset -= 0.66;
        }
    }

    Mesh::new(vertices, indices)
}

use std::f32::EPSILON;
const THICKNESS: &[f32] = &[
    0.002,
    0.007,
];
const COLORS: &[[f32; 4]] = &[
    [1.0, 0.3, 0.7, 1.0],
    [0.3, 0.6, 1.0, 1.0],
];
const SIZE: f32 = 0.5;

/* 
I know nothing about licenses but I say that this license only applies to the code beneath it
The rest is still GPLV3
*/

/*
MIT/X Consortium License

@ 2019-2021 Fedor Logachev <not.fl3@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
*/

// https://github.com/not-fl3/macroquad/blob/master/src/shapes.rs#L163
pub fn from_lissajous(points: Vec<Vec<[f32; 2]>>) -> Mesh {
    let config = config::get().lissajous;
    let resolution = config::get_resolution();
    let y_stretch = resolution.0 as f32 / resolution.1 as f32;

    let mut vertices: Vec<Vertex> = Vec::new();
    let mut indices: Vec<u32> = Vec::new();

    for (level, points) in points.iter().enumerate() {
        let points = points.clone();
        let mut color = *COLORS.get(level).unwrap_or(&[1.0, 0.0, 0.0, 1.0]);
        color[3] *= config.alpha;

        let thickness = *THICKNESS.get(level).unwrap_or(&0.002);

        let mut points = points.iter().peekable();
        loop {
            let p1 = match points.next() {
                Some(p) => p,
                None => break,
            };
            let p2 = match points.peek() {
                Some(p) => p,
                None => break,
            };

            let x1 = p1[0] * SIZE;
            let y1 = p1[1] * SIZE * y_stretch;
            let x2 = p2[0] * SIZE;
            let y2 = p2[1] * SIZE * y_stretch;

            let dx = x2 - x1;
            let dy = y2 - y1;

            let nx = -dy;
            let ny = dx;

            let tlen = (nx * nx + ny * ny).sqrt() / (thickness * 0.5);
            if tlen < EPSILON {
                continue;
            }
            let tx = nx / tlen;
            let ty = ny / tlen;

            let offset = vertices.len() as u32;
            vertices.push(Vertex{ position: [x1 + tx, y1 + ty, 0.0], color});
            vertices.push(Vertex{ position: [x1 - tx, y1 - ty, 0.0], color});
            vertices.push(Vertex{ position: [x2 + tx, y2 + ty, 0.0], color});
            vertices.push(Vertex{ position: [x2 - tx, y2 - ty, 0.0], color});

            indices.append(&mut vec![
                offset + 0,
                offset + 1,
                offset + 2,
                offset + 2,
                offset + 1,
                offset + 3
            ]);
        }
    }

    Mesh::new(vertices, indices)
}