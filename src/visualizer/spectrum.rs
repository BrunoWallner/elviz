use crate::graphics::Mesh;
use super::mesh;

use audioviz::spectrum::{Frequency, stream::Stream};

pub struct Spectrum {
    pub stream: Stream,
}
impl Spectrum {
    pub fn update(&mut self) -> Mesh {
        self.stream.update();

        let frequencies: Vec<Vec<Frequency>> = self.stream.get_frequencies();
        let frequencies: Vec<Frequency> = if frequencies.len() >= 2 {
            let mut buf: Vec<Frequency> = Vec::new();
    
            // left
            let mut left = frequencies[0].clone();
            left.reverse();
            buf.append(&mut left);
    
            // right
            buf.append(&mut frequencies[1].clone());
    
            buf
        } else {
            if frequencies.len() == 1 {
                let mut buf: Vec<Frequency> = Vec::new();
                
                // left
                let mut left = frequencies[0].clone();
                left.reverse();
                buf.append(&mut left);

                // right
                buf.append(&mut frequencies[0].clone());

                buf
            } else {
                Vec::new()
            }
        };

        mesh::from_frequencies(frequencies)
    }

    pub fn push_data(&mut self, data: Vec<f32>) {
        self.stream.push_data(data);
    }
}