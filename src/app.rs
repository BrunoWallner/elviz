use std::sync::mpsc;

use crate::graphics::State;
use crate::graphics::Mesh;
use crate::window::get as get_window;
use crate::window::Options;
use crate::config;

use winit::{
    event::*,
    event_loop::{ControlFlow, EventLoop},
};

pub enum Instruction {
    SetMesh(Mesh)
}

pub async fn run<'a>(window_options: &Options, instruction_receiver: mpsc::Receiver<Instruction>) -> Result<(), ()> {
    let event_loop = EventLoop::new();
    let window = get_window(&event_loop, window_options).ok_or(())?;

    // State::new uses async code, so we're going to wait for it to finish
    let mut state = State::new(&window).await;

    event_loop.run(move |event, _, control_flow| {
        let config = config::get();
        match event {
            Event::WindowEvent {
                ref event,
                window_id,
            } if window_id == window.id() => {
                if !state.input(event) {
                    match event {
                        WindowEvent::CloseRequested => {
                            *control_flow = ControlFlow::Exit;
                        }
                        WindowEvent::KeyboardInput{input, ..} =>  {
                            match input {
                                KeyboardInput {state, virtual_keycode, .. } => {
                                    if state == &ElementState::Released {
                                        match virtual_keycode {
                                            Some(code) => {
                                                if code == &VirtualKeyCode::Escape {
                                                    *control_flow = ControlFlow::Exit;
                                                }
                                                if code == &VirtualKeyCode::F11 {
                                                    crate::window::toggle_f11(&window);
                                                }
                                            }
                                            None => (),
                                        }
                                    }
                                }
                            }
                        },
                        WindowEvent::Resized(physical_size) => {
                            state.resize(*physical_size);
                            config::set_resolution((physical_size.width, physical_size.height));
                        }
                        WindowEvent::ScaleFactorChanged { new_inner_size, .. } => {
                            // new_inner_size is &mut so w have to dereference it twice
                            state.resize(**new_inner_size);
                            config::set_resolution((new_inner_size.width, new_inner_size.height));
                        }
                        _ => {}
                    }
                }
            }
            Event::RedrawRequested(window_id) if window_id == window.id() => {
                // receive Instructions
                for instruction in instruction_receiver.try_iter() {
                    match instruction {
                        Instruction::SetMesh(mesh) => {
                            state.set_mesh(mesh);
                        }
                    }
                }
                state.update();
                match state.render(config.black_background) {
                    Ok(_) => {}
                    // Reconfigure the surface if it's lost or outdated
                    Err(wgpu::SurfaceError::Lost | wgpu::SurfaceError::Outdated) => state.resize(state.size),
                    // The system is out of memory, we should probably quit
                    Err(wgpu::SurfaceError::OutOfMemory) => *control_flow = ControlFlow::Exit,
                    // We're ignoring timeouts
                    Err(wgpu::SurfaceError::Timeout) => (),
                }
            }
            Event::MainEventsCleared => {
                // RedrawRequested will only trigger once, unless we manually
                // request it.
                window.request_redraw();
            }
            _ => {}
        }
    });
}