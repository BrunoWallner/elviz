mod window;
mod graphics;
mod app;
mod visualizer;
mod config;
mod logger;

use std::sync::mpsc;
use std::process::exit;
use std::time::Duration;

use clap::{Command, Arg, AppSettings};
use std::path::Path;
use directories::BaseDirs;

fn main() {
    let _logger = logger::setup().unwrap();

    let matches = Command::new("audiovis")
        .version("0.1.0")
        .author("Luca Biendl <b.lucab1211@gmail.com>")
        .about("tool to visualize audio")
        .setting(AppSettings::ColorAlways)
        .setting(AppSettings::ColoredHelp)

        .arg(Arg::with_name("monitors")
            .short('m')
            .long("monitors")
            .takes_value(false)
            .help("lists all available monitor id's"))

        .arg(Arg::with_name("generate-default-config")
            .short('g')
            .long("generate-default-config")
            .takes_value(false)
            .help("generates the default config into 'config.toml'"))

        .arg(Arg::with_name("config")
            .short('c')
            .long("config")
            .takes_value(true)
            .help("path of config"))

        .get_matches();

    if matches.is_present("monitors") {
        window::print_monitor_ids();
        exit(0);
    }

    let config_path = BaseDirs::new()
        .unwrap()
        .config_dir()
        .join("elviz")
        .join("config.toml");

    if matches.is_present("generate-default-config") {
        config::generate(&config_path);
        exit(0);
    }
    match matches.value_of("config") {
        Some(c) => config::init(&Path::new(c)),
        None => {
            if config_path.exists() {
                config::init(&config_path)
            } else {
                config::generate(&config_path);
                config::init(&config_path)
            }
        }
    }

    let always_on_top = config::get().always_on_top;
    let monitor_id = config::get().monitor_id;

    let window_options = window::Options {
        monitor_id,
        always_on_top,
        ..window::Options::default()
    };

    let (instruction_tx, instruction_rx) = mpsc::channel();
    let tick_rate = config::get().tick_rate;
    visualizer::init(instruction_tx, Duration::from_millis(1000 / tick_rate));

    pollster::block_on(app::run(&window_options, instruction_rx)).unwrap();
}