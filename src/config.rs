use std::fs::create_dir_all;
use std::fs::remove_dir;
use std::fs::remove_file;
use std::fs::File;
use std::io::{Read, Write};
use std::path::Path;
use std::process::exit;

use audioviz::spectrum::config::Interpolation;
use serde::{Deserialize, Serialize};

pub static mut CONFIG: Config = Config::default();

pub fn get() -> Config {
    unsafe { CONFIG.clone() }
}

pub fn init(path: &Path) {
    unsafe {
        CONFIG = Config::get(path);
    }
}

pub static mut RESOLUTION: (u32, u32) = (0, 0);

pub fn get_resolution() -> (u32, u32) {
    unsafe { RESOLUTION }
}
pub fn set_resolution(resolution: (u32, u32)) {
    unsafe { RESOLUTION = resolution }
}

// generates default config
pub fn generate(path: &Path) {
    // make sure that path is valid
    let mut dir_path = path.to_path_buf();
    dir_path.pop();
    create_dir_all(dir_path).unwrap();

    // delete if already exists
    if path.exists() {
        if path.is_file() {
            remove_file(path).unwrap();
        } else if path.is_dir() {
            remove_dir(path).unwrap();
        }
    }

    let mut file = File::create(path).unwrap();
    let default = Config::default();
    let serialized = serde_json::to_string_pretty(&default).unwrap();
    file.write_all(serialized.as_bytes()).unwrap();

    log::info!("generated default config at: {:?}", path);
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Spectrum {
    pub bar_width: f32,
    pub y_offset: f32,
    pub y_multiplier: f32,
    pub alpha: f32,
    pub fft_resolution: usize,
    pub amount: Option<usize>,
    pub gravity: Option<f32>,
    pub enabled: bool,
    pub frequency_bounds: [usize; 2],
    pub interpolation: Interpolation,
}
impl Spectrum {
    pub const fn default() -> Self {
        Self {
            bar_width: 1.0,
            y_offset: 0.0,
            y_multiplier: 1.0,
            alpha: 0.5,
            fft_resolution: 2500,
            amount: Some(200),
            gravity: Some(1.8),
            enabled: true,
            frequency_bounds: [50, 20_000],
            interpolation: Interpolation::Cubic,
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Lissajous {
    pub buffer_length: usize,
    pub resolution_reduction: u32,
    pub alpha: f32,
    pub enabled: bool,
}
impl Lissajous {
    pub const fn default() -> Self {
        Self {
            buffer_length: 2048,
            resolution_reduction: 2,
            alpha: 0.5,
            enabled: true,
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Config {
    pub volume: f32,
    pub black_background: bool,
    pub override_resolution: Option<(u32, u32)>,
    pub tick_rate: u64,
    pub monitor_id: usize,
    pub always_on_top: bool,
    pub spectrum: Spectrum,
    pub lissajous: Lissajous,
}
impl Config {
    // delays startup time
    fn get(path: &Path) -> Self {
        let mut file = match File::open(path) {
            Ok(f) => f,
            Err(e) => {
                log::warn!("failed to load config: {}", e);
                exit(1);
            }
        };
        let mut content = vec![];
        file.read_to_end(&mut content).unwrap();

        log::info!("loaded config from: {:?}", path);

        // toml::from_slice(&content),
        serde_json::from_slice(&content).unwrap()
    }
    pub const fn default() -> Self {
        Self {
            volume: 1.0,
            black_background: false,
            override_resolution: None,
            tick_rate: 60,
            monitor_id: 0,
            always_on_top: false,
            spectrum: Spectrum::default(),
            lissajous: Lissajous::default(),
        }
    }
}
