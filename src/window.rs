use winit::window::Window;
use winit::window::WindowBuilder;
use winit::event_loop::EventLoop;
use winit::dpi::{PhysicalSize, PhysicalPosition};

use crate::config;


use nsvg::parse_str;
use nsvg::Units;

#[cfg(unix)]
use winit::platform::unix::WindowExtUnix;
#[cfg(unix)]
use winit::window::Theme;

pub struct Options {
    pub monitor_id: usize,
    pub always_on_top: bool,
    pub decorations: bool,
}
impl Options {
    pub fn default() -> Self {
        Self {
            monitor_id: 0,
            always_on_top: true,
            decorations: false,
        }
    }
}

pub fn get(event_loop: &EventLoop<()>, options: &Options) -> Option<Window> {
    let window = WindowBuilder::new()
        .with_transparent(true)
        // only works on windows and X11
        .with_always_on_top(options.always_on_top)
        .build(&event_loop)
        .unwrap();

    let _ = window.set_cursor_hittest(false);

    #[allow(unused_mut)]
    let size = if let Some(size) = config::get().override_resolution {
        PhysicalSize {width: size.0, height: size.1}
    } else {
        match window.available_monitors().nth(options.monitor_id) {
            Some(monitor) => {
                monitor.size()
            }
            None => PhysicalSize{width: 1920, height: 1080}
        }
    };

    window.set_inner_size(size);
    config::set_resolution((size.width, size.height));

    window.set_decorations(options.decorations);

    window.set_outer_position(PhysicalPosition::new(0, 0));

    // dark client side rendering on unix wayland
    #[cfg(unix)]
    window.wayland_set_csd_theme(Theme::Dark);

    let icon = include_str!("../media/Elviz.svg");
    let inch_in_mm = 25.4;
    let icon_size = 100.0; // in mm wtf are inches
    let icon_size = icon_size / inch_in_mm; // in inches :(
    let desired_total_dots = 32.0;
    let dpi = desired_total_dots / icon_size;
    if let Ok(icon) = parse_str(icon, Units::Pixel, dpi) {
        if let Ok(icon_image) = icon.rasterize(1.0) {
            if let Ok(icon) = winit::window::Icon::from_rgba(icon_image.into_raw(), desired_total_dots as u32, desired_total_dots as u32) {
                window.set_window_icon(Some(icon));
                log::info!("set window icon");
            }
        }
    }

    Some(window)
}

pub fn toggle_f11(window: &Window) {
    if !window.is_decorated() {
        window.set_decorations(true);

        // forces size update
        // so that in gnome top bar is visible
        let size = window.inner_size();
        window.set_inner_size(size);

        let _ = window.set_cursor_hittest(true);
    } else {
        window.set_decorations(false);
        let _ = window.set_cursor_hittest(false);

        // forces update
        let size = window.inner_size();
        window.set_inner_size(size);
    }
}

pub fn print_monitor_ids() {
    let event_loop = EventLoop::new();
    let window = WindowBuilder::new()
        .build(&event_loop)
        .unwrap();

    let monitors = window.available_monitors();
    println!("┌─────┬────────────┬─────────────────────┐");
    println!("│ ID  │ resolution │ name                │");
    println!("├─────┼────────────┼─────────────────────┤");
    for (id, mon) in monitors.enumerate() {
        let mut name = mon.name().unwrap_or(String::from(""));
        let res = mon.size();
        let mut res = format!("{}x{}", res.width, res.height);

        if name.len() > 19 {
            name = name.chars().collect::<Vec<char>>()[0..19]
                .iter().cloned().collect();
        }
        if res.len() > 9 {
            res = res.chars().collect::<Vec<char>>()[0..9]
            .iter().cloned().collect();
        }

        println!("│ {:<3} │ {:<10} │ {:<19} │", id, res, name);
    }
    println!("└─────┴────────────┴─────────────────────┘");

}