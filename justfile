build:
    @cargo build --release

build_windows:
    @cargo build --release --target x86_64-pc-windows-gnu

run:
    @cargo run --release

install:
    cp ./target/release/elviz /usr/bin/elviz
    cp ./media/Elviz.desktop /usr/share/applications/Elviz.desktop
    cp ./media/Elviz.svg /usr/share/pixmaps/Elviz.svg

uninstall:
    rm -rf /usr/share/applications/Elviz.desktop
    rm -rf /usr/share/pixmaps/Elviz.svg

sloc:
    @find src -name "*.rs" -print | xargs wc -l 
